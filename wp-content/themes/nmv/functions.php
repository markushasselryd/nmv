<?php

// =============================================================================
// FUNCTIONS.PHP
// -----------------------------------------------------------------------------
// Overwrite or add your own custom functions to X in this file.
// =============================================================================

// =============================================================================
// TABLE OF CONTENTS
// -----------------------------------------------------------------------------
//   01. Enqueue Parent Stylesheet
//   02. Additional Functions
// =============================================================================

// Enqueue Parent Stylesheet
// =============================================================================

// add_filter( 'x_enqueue_parent_stylesheet', '__return_true' );



// Enqueue and register styles and scripts
// =============================================================================

//Remove default child style
function nmv_remove_styles(){
	wp_dequeue_style('x-child');

	wp_deregister_style('x-child');
}

add_action('wp_print_styles', 'nmv_remove_styles');

//Add styles and scripts
function nmv_add_styles_and_scripts(){

	// Register styles
	wp_register_style('owlcarousel', get_stylesheet_directory_uri() . '/library/vendor/owlCarousel/owl.carousel.min.css', '', '2.2');
	// wp_register_style('owl-theme', get_stylesheet_directory_uri() . '/library/vendor/owlCarousel/owl.theme.default.min.css', 'owlcarousel', '2.2');

	//Enqueue the parent style
	wp_enqueue_style('parent', get_template_directory_uri() . '/framework/css/dist/site/stacks/renew.css');

	//Enqueue the child style
	wp_enqueue_style('nmv', get_stylesheet_directory_uri() . '/library/css/style.css');

	// Register scripts
	wp_register_script('nmv-js', get_stylesheet_directory_uri() . '/library/js/nmv-min.js', 'jquery', true);
	wp_register_script('owlcarousel', get_stylesheet_directory_uri() . '/library/vendor/owlCarousel/owl.carousel.min.js', 'jquery', '2.2', true);

	// Enqueue scripts
	wp_enqueue_script('nmv-js');

	if( is_front_page() ){
		wp_enqueue_script('owlcarousel');
		wp_enqueue_style('owlcarousel');
		// wp_enqueue_style('owl-theme');
	}
}

add_action('wp_enqueue_scripts', 'nmv_add_styles_and_scripts', 9999);

// Head clean up
// =============================================================================

function head_cleanup(){
	//Remove emoji support
	remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
	remove_action( 'wp_print_styles', 'print_emoji_styles' );

	//Remove RSS feeds
	remove_action('wp_head', 'feed_links', 2);
	remove_action('wp_head', 'feed_links_extra', 3);

	//Disable REST API
	remove_action( 'wp_head', 'rest_output_link_wp_head', 10 );
}

add_action('after_setup_theme', 'head_cleanup');

// function x_output_generated_styles() {
//  //Empty
// }

// Includes
// =============================================================================
require_once( get_stylesheet_directory() . '/includes/recent-posts.php');
require_once( get_stylesheet_directory() . '/includes/post-meta.php');
require_once( get_stylesheet_directory() . '/includes/shortcodes.php');

// Add footer illustration
// =============================================================================
function add_footer_divider(){
	?>
	<div class="section-divider"></div>
<?php }

add_action('x_before_footer_content', 'add_footer_divider');

// Password protect "Medarbetarportal"
// =============================================================================

function protect_medarbetarportalen( $content ){
	if( is_page_template('template-portal.php') )
		$content = '[x_protect heading="Lösenordsskyddat" ]' . $content . '[/x_protect]';
	return $content;
}

add_filter( 'the_content', 'protect_medarbetarportalen');

// Load translations
// =============================================================================
add_action( 'after_setup_theme', 'load_child_language' );
	function load_child_language() {
		load_child_theme_textdomain( '__x__', get_stylesheet_directory() . '/languages' );
}

// OwlCarousel

function add_owlcarousel(){
	if( is_front_page() ){ ?>
		<script type="text/javascript">

		(function($){
			$('.hero-slider').owlCarousel({
				items:1,
				lazyLoad: true,
				nav: true,
				loop: true,
				animateOut: 'fadeOut',
				autoplay: true,
				autoplayTimeout: 5000,
				dots: false
			});

			$('.certificates-container').owlCarousel({
				items:3,
				nav: true,
				dots: false,
				autoWidth: true,
				margin: 50,
				stagePadding: 50,
				navText: ["<i class='x-icon x-icon-angle-left'>", "<i class='x-icon x-icon-angle-right'>"]
			});


		})(jQuery);

	</script>
	<?php }
}

add_action('wp_footer', 'add_owlcarousel', 100);
