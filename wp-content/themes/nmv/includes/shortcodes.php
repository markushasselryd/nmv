<?php

function staff_shortcode(){

  // Replace åäö to be able to use it
  $search = array('å', 'ä', 'ö', 'Å', 'Ä', 'Ö');
  $replace = array('a', 'a', 'o', 'A', 'A', 'O');

  // Check if there are any city posts
  if( have_rows( 'city' )):

    // Create the variable that stores the content
    if( empty( $staff_content )){
      $staff_content = "";
    }
    $staff_content .= ('<section class="x-section no-padding">');
    $staff_content .= ('<div class="x-container max width">');
    $staff_content .= ('<ul class="location-menu">');

    // Start the loop and create the menu
    while( have_rows( 'city') ): the_row();
      $staff_content .= sprintf('<li><a href="?" data-link="#%1$s">%2$s</a></li>', strtolower( str_replace( $search, $replace, get_sub_field( 'city_name') ) ), get_sub_field( 'city_name')  );
    endwhile;

    // Close the list
    $staff_content .= ('</ul>');

    // Close container
    $staff_content .= ('</div>');

    // Close section
    $staff_content .= ('</section>');

  endif;

  // Output the cities and staff
  if( have_rows( 'city') ):

    while( have_rows( 'city') ): the_row();

      // vars
      $city_address = get_sub_field( 'city_address' );
      $city_zipcode = get_sub_field( 'city_zipcode' );
      $city_name = get_sub_field( 'city_name' );
      $city_phone = get_sub_field( 'city_phone' );
      $city_email = get_sub_field( 'city_email' );

      $staff_content .= sprintf('<section id="%s" class="contact-city tab-content x-section" style="background-color:#fff;">', strtolower( str_replace( $search, $replace, $city_name ) ) );
      $staff_content .= ('<div class="x-container max width">');
      $staff_content .= sprintf('<h2>%s</h2>', $city_name );
      $staff_content .= sprintf('<ul class="x-block-grid four-up">');

      // Print city details
      $staff_content .= sprintf('<li class="x-block-item city-content">
        <h4>Kontor</h4>
        %1$s<br />
        %2$s %3$s
        <ul class="x-ul-icons">
        <li class="x-li-icon"><i class="x-icon-phone"></i>%4$s</li>
        <li class="x-li-icon"><i class="x-icon-envelope-o"></i><a href="mailto:%5$s">%5$s</a></li>
        </ul>
      </li>',
      $city_address,
      $city_zipcode,
      $city_name,
      $city_phone,
      $city_email);

      if( have_rows( 'personnel' ) ):
        while( have_rows( 'personnel' ) ): the_row();

          // vars
          $image = get_sub_field( 'staff_image' );
          $name = get_sub_field( 'staff_name' );
          $role = get_sub_field( 'staff_role' );
          $phone = get_sub_field( 'staff_phone' );
          $email = get_sub_field( 'staff_email' );

          $staff_content .= ('<li class="x-block-item">');
          $staff_content .= sprintf('<img src="%1$s" class="size-full" alt="%2$s">', $image['url'], $image['alt']);
          $staff_content .= ('<div class="staff-content">');
          $staff_content .= sprintf('<h5>%s</h5>', $name);
          $staff_content .= sprintf('<span class="staff-role">%s</span><br />', $role);

          // Check if person has phone or email registered
          if( $phone || $email):
            $staff_content .= ('<ul class="x-ul-icons">');

            if( $phone ){
                $staff_content .= sprintf('<li class="x-li-icon"><i class="x-icon-phone"></i>%s</li>', $phone);
            }
            if( $email ){
                $staff_content .= sprintf('<li class="x-li-icon"><i class="x-icon-envelope-o"></i><a href="mailto:%1$s">%2$s</a></li>', $email, $email);
            }

            $staff_content .= ('</ul>');
          endif;


          // Close staff content
          $staff_content .= ('</div>');

          // close block item
          $staff_content .= ('</li>');

        endwhile;
      endif;

      //Close block grid
      $staff_content .= ('</ul>');
      // Close container
      $staff_content .= ('</div>');
      // Close city section
      $staff_content .= ('</section>');

    endwhile;
  endif;

  return $staff_content;


}

add_shortcode('staff', 'staff_shortcode');

// Slider on start page
function slider_shortcode(){

  if( have_rows( 'slider' ) ):

    if( empty( $slider_content ) ){
      $slider_content = "";
    }


    $slider_content .= ('<div class="hero-slider owl-theme owl-carousel">');

    while( have_rows( 'slider' ) ): the_row();

      // Vars
      $image = get_sub_field('image');
      $header = get_sub_field('header');
      $subheader = get_sub_field('subheader');
      $link = get_sub_field('link');

      $slider_content .= sprintf('<div class="hero-slider-item owl-lazy" data-src="%s">', $image['url']);
      $slider_content .= ('<div class="hero-slider-item-content negative-text">');
      $slider_content .= sprintf('<h1 class="h-custom-headline h-line">%s</h1>', $header);
      $slider_content .= sprintf('<div class="x-text lead-in">%s</div>', $subheader);
      $slider_content .= sprintf('<a href="%s" class="x-btn x-btn-global">Våra tjänster</a>', $link);

      // Close hero-slider-item-content
      $slider_content .= ('</div>');
      // Close hero-slider-item
      $slider_content .= ('</div>');

    endwhile;

    //Close hero-slider-container
    $slider_content .= ('</div>');

    return $slider_content;


  endif;

}

add_shortcode('slider_homepage', 'slider_shortcode');

// Certificates on startpage
function certificates_shortcode(){

  if( have_rows( 'certifications' ) ):

    if( empty( $certificate_content ) ){
      $certificate_content = "";
    }

    $certificate_content .= ('<div class="certificates-container owl-carousel">');

    while( have_rows( 'certifications' ) ): the_row();

      // vars
      $image = get_sub_field('image');
      $url = get_sub_field('url');
      $use_custom_imageurl = get_sub_field('use_custom_image_url');
      $custom_imageurl = get_sub_field('custom_image_url');
      $image_size = 'thumbnail';


      $certificate_content .= ('<div class="certificate-item">');

      if( $use_custom_imageurl == true ){
        $certificate_content .= sprintf('<a href="%1$s" target="_blank"><img src="%2$s"></a>', $url ,$custom_imageurl);
      } else {
        $certificate_content .= sprintf('<a href="%1$s" target="_blank"><img src="%2$s" alt="%3$s" width="%4$s" height="%5$s"></a>', $url, $image['url'], $image['alt'], $image['sizes'][$image_size . '-width'], $image['sizes'][$image_size . '-height'] );
      }

      // Close certificate-item
      $certificate_content .= ('</div>');

    endwhile;

    $certificate_content .= ('</div>');

  endif;

  return $certificate_content;

}

add_shortcode('certificates', 'certificates_shortcode');
