jQuery(document).ready(function($){

  var shrinkStart = $('.x-navbar').height();

  $(window).on('scroll', function(){
    if ( $(document).scrollTop() > shrinkStart ){
      $('.x-navbar').addClass('shrink');
    } else {
      $('.x-navbar').removeClass('shrink');
    }
  });

  //Filtering contact page

  // var $btns = $('.location-menu a').click(function(e) {
  //   e.preventDefault();
  //
  //   // if (this.id == 'all') {
  //   //   $('#parent > div').fadeIn(450);
  //   // }
  //
  //     var $el = $(this.data('link')).fadeIn(450);
  //     $('.tab-content > section').not($el).hide();
  //
  //   $btns.removeClass('active');
  //   $(this).addClass('active');
  // });

  // $('.tab-content').hide();
  // var startOpen = $('.location-menu a');
  // if( startOpen.data('link') === "#sundsvall"){
  //     $(startOpen).show();
  //     $(startOpen).parent().addClass("current");
  // }



  // var startOpen = $('.location-menu a').data('link');
  // if( startOpen == "#sundsvall"){
  //   $('.location-menu a').addClass("current");
  //
  // }

  $('.tab-content').hide();
  $('.location-menu a[data-link="#sundsvall"]').parent().addClass('current');
  $('#sundsvall.tab-content').fadeIn(450);

  // if( startOpen.data('link') === "#sundsvall"){
  //   $(startOpen).data('link', '#sundsvall').parent().addClass('current');
  // }

  $(".location-menu a").click(function(e) {
			e.preventDefault();
      $(this).parent().addClass("current");
      $(this).parent().siblings().removeClass("current");
      var section = $(this).attr("data-link");
      $(".tab-content").not(section).css("display", "none");
      $(section).fadeIn(450);
  });


});
