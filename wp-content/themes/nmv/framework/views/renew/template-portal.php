<?php

// =============================================================================
// VIEWS/RENEW/TEMPLATE-PORTAL.PHP
// -----------------------------------------------------------------------------
// Sidebar left, content right page output for Renew.
// =============================================================================

?>

<?php get_header(); ?>

  <div class="x-container max width portal-content">
    <div class="<?php x_main_content_class(); ?>" role="main">

      <?php while ( have_posts() ) : the_post(); ?>
        <?php x_get_view( 'renew', 'content', 'page' ); ?>
        <?php x_get_view( 'global', '_comments-template' ); ?>
      <?php endwhile; ?>

    </div>

    <?php get_sidebar(); ?>

    <?php if( is_user_logged_in() ){ ?>
      <a href="<?php echo wp_logout_url( home_url() ); ?>" class="x-btn logout">Logga ut</a>
    <?php } ?>
  </div>

<?php get_footer(); ?>
