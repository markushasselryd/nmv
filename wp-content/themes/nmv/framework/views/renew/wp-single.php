<?php

// =============================================================================
// VIEWS/RENEW/WP-SINGLE.PHP
// -----------------------------------------------------------------------------
// Single post output for Renew.
// =============================================================================

$fullwidth = get_post_meta( get_the_ID(), '_x_post_layout', true );

?>

<?php get_header(); ?>

  <div class="x-container max width offset">
    <div class="x-main full" role="main">

      <?php while ( have_posts() ) : the_post(); ?>
        <?php x_get_view( 'renew', 'content', get_post_format() ); ?>
        <?php x_get_view( 'global', '_comments-template' ); ?>
      <?php endwhile; ?>
      <div class="entry-extra">
        <?php echo do_shortcode('[share title="Dela inlägget" facebook="true" twitter="true" google_plus="true" linkedin="true" email="true"]'); ?>
      </div>
    </div>

    
  </div>

  <div class="x-section" style="background-color:#fff;">
    <div class="x-container max-width">
      <div class="x-column x-1-1">
          <h2 class="center-text mbm">Senaste inläggen</h2>
      </div>
    </div>
    <div class="x-container max width">
      <div class="x-column x-1-2 x-sm">

        <?php if( is_singular('miljovardsmagasinet') ){
          echo do_shortcode('[x_recent_posts type="miljovardsmagasinet" count="2" orientation="vertical" no_image="true" show_excerpt="true"]');
          } else {
          echo do_shortcode('[x_recent_posts count="2" orientation="vertical" no_image="true" show_excerpt="true"]');
          }
        ?>

      </div>
      <div class="x-column x-1-2 x-sm">

        <?php if( is_singular('miljovardsmagasinet') ){
          echo do_shortcode('[x_recent_posts type="miljovardsmagasinet" offset="2" count="2" orientation="vertical" no_image="true" show_excerpt="true"]');
          } else {
          echo do_shortcode('[x_recent_posts count="2" offset="2" orientation="vertical" no_image="true" show_excerpt="true"]');
          }
        ?>

      </div>
    </div>

  </div>

<?php get_footer(); ?>
