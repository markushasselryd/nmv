<?php

// =============================================================================
// VIEWS/GLOBAL/_NAV-PRIMARY.PHP
// -----------------------------------------------------------------------------
// Outputs the primary nav.
// =============================================================================

if( function_exists( 'ubermenu' ) && $config_id = ubermenu_get_menu_instance_by_theme_location( 'primary' ) ):
	ubermenu( $config_id, array( 'theme_location' => 'primary') );
 else: ?>

<nav class="x-nav-wrap desktop" role="navigation">
  <?php x_output_primary_navigation(); ?>
</nav>

<?php endif; ?>
