<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'nmv' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '+o;jU6 kFp|X+ky`+1rp`X~$2|xS-aa`1YyC:C0VP|*E)fJV#L,;ILA)>+E6jF$&');
define('SECURE_AUTH_KEY',  '|q9Xp[eW*tz3td|)4_+gR$f?m=08:$6}8]-^qG,4i9J2RRY/x5lN=(9k+8&jg`HK');
define('LOGGED_IN_KEY',    'A+*e,]Z9R6UEOOV|qsk7@q/:@t$%G[3i{TcM_5|u8B9;G5oVI_70;92~Pc8B_$/N');
define('NONCE_KEY',        'MI-:e+wXq y{F+78 t`H}/R4MPMl+qDR+(5+0$]&=n)-NGv+%1!<F!^N0N^>S+(?');
define('AUTH_SALT',        'zl_ *H< WJ6jPggQ&4j5$B25+C-1A|-XrJ2I+<_TGz$7Y%+orHvl0^RBVLBswBWo');
define('SECURE_AUTH_SALT', 'obdZv}wI]r :-1N4OsR3zp]][(R:0,(mGjsk_2+-V^jB*(5y|mRC.uDO6_W|X?wB');
define('LOGGED_IN_SALT',   '[=e1KBZ%C~@si|oF O]}gCRE{-Gqa3K&jLl:c!YctON-yjVBjK.*_stY@k2+GJFG');
define('NONCE_SALT',       'UohX?/Re/l*_YT0|JS >(9viKRu=6shf?_ Q?~-vof5Q.pDZ<O?4OC9:o_1`wgE_');


/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';


define('WP_DEBUG', true);



/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) )
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
